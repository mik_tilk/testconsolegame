//
//  main.m
//  NIXMacProject
//
//  Created by Sergey on 10/2/15.
//  Copyright © 2015 Nixsolutions. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
